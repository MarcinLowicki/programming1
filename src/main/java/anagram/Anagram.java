package anagram;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Anagram {

    public static void main(String[] args) {


        Scanner scannerAnagram = new Scanner(System.in);
        System.out.println("Podaj dwaslowa a sprawdze czy są anagramami: ");
        System.out.println("First Word: ");
        String firstWord = scannerAnagram.nextLine();
        System.out.println("Second Word: ");
        String secondWord = scannerAnagram.nextLine();

        char[] charsFirst = firstWord.toCharArray();
        char[] charsSecond = secondWord.toCharArray();
        Arrays.sort(charsFirst);
        Arrays.sort(charsSecond);

        boolean equals = charsFirst.equals(charsSecond);

        if (equals == true) {

            System.out.println("Slowo jest anagramem :" + equals);


        } else {
            System.out.println("Slowo " + equals + " NIE jest anagramem");

        }
    }
}
