package palindrom;

import java.util.Scanner;

public class Palindrom {

    public static void main(String[] args) {

        boolean isPalindrom = true;
        Scanner scannerWords = new Scanner(System.in);
        System.out.println("Wczytaj slowo: ");
        String stringWordCheck = scannerWords.nextLine();
        System.out.println(stringWordCheck);

        int start = 0;
        int end = stringWordCheck.length()-1;

        while (start <= end) {
            if (stringWordCheck.charAt(start) != stringWordCheck.charAt(end)) {
                isPalindrom = false;
                break;
            }
            start++;
            end--;
        }
        if (isPalindrom) {
            System.out.println("Słowo" + stringWordCheck + " nie jest palindromem");
        } else {
            System.out.println("Slowo " + stringWordCheck + " jest palindromem");
        }
    }

}

