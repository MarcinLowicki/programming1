package silnia_factorial;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {

        System.out.println("Podaj n-silnie");
        Scanner scanner = new Scanner(System.in);
        int nextI = scanner.nextInt();
        System.out.println(factorial(nextI));
    }

    static long factorial(int n) {

        if (n >= 1) {
            return n * factorial(n - 1);
        } else {
            return 1;
        }
    }
}
