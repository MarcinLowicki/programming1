package chess;

import java.util.Scanner;

public class Chess {

    public static final String BLACK = " \u25A0 ";
    public static final String WHITE = " \u25A2 ";

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj wielkosc szachownicy: ");
        int chess_lenght = scan.nextInt();
        System.out.println(chess_lenght);
        System.out.println("Rysujesz szachownice o dlugosci " + chess_lenght);

        for (int i = 0; i < chess_lenght; i++) {
            //powyzsza petla schodzi w dol

            for (int j = 0; j < chess_lenght; j++) {

                if ((i + j) % 2 == 0) {
                    System.out.print(BLACK + " ");
                } else {
                    System.out.print(WHITE + " ");
                }
            }
            System.out.println("");
        }
    }
}
