package adventOfCode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day1 {

    // https://adventofcode.com/
    //masa /3-> wynik zaokraglic w dol -2 = to nam da PALIWO.
    // dla kazdego wpisu z pliku liczymy paliwo
    // nastepnie sumujemy wszystko

    //PART 2

    public static void main(String[] args) throws IOException {

        Stream<String> input = Files.lines(
                Path.of("src/main/resources/adventOfCode/day1input.txt"));
        List<String> listOfModuleMass = input.collect(Collectors.toList());

        int sum = 0;
        for (String mass:listOfModuleMass) {
            int requiredFuel = calculateFuel(Integer.parseInt(mass));
            sum+=requiredFuel;
            System.out.println("Fueal needed for mass" + mass + "->>" + requiredFuel );
            System.out.println("Suma of Fuel: "+ sum);
        }
        //System.out.println(listOfModuleMass)d;

    }

    static public int calculateFuel(int mass) {
        return (mass / 3) - 2;


}

}
