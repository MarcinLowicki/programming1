package calculatorEnum;

import javax.swing.*;

public enum Example {

    RED("Czer") {
        @Override
        public void someMethod() {
            System.out.println("Something RED");
        }
    },
    GREEN("Ziel") {
        @Override
        public void someMethod() {
            System.out.println("Something BLUE");
           }
    },
    BLUE("Nieb") {
        @Override
        public void someMethod() {
            System.out.println("Something BLUE");
        }
    },
    WHITE("Bia") {
        @Override
        public void someMethod() {
            System.out.println("Something WHITE");
        }
    };

    public abstract void someMethod();
    private final String desc;

    Example(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;

    }

}
