package calculatorEnum;

import org.w3c.dom.ls.LSOutput;

import java.util.function.IntBinaryOperator;

//import static com.sun.tools.javac.main.Option.X;

public enum Calculator {


    ADD("Dodawanie", new IntBinaryOperator() {
        @Override
        public int applyAsInt(int left, int right) {
            return left + right;
        }
    }),
    MULTIPLY("Mnożenie", (x, y) -> x * y),
    SUBTRACT("Odejmowanie", (x, y) -> x - y);


    //zmienna
    private final String desc;
    private IntBinaryOperator operator;//interfejs
    //constructor

    Calculator(String desc, IntBinaryOperator operator) {
        this.desc = desc;
        this.operator = operator;
    }

    //metody - astrakcyjna
    public int calculate(int a, int b) {
        return operator.applyAsInt(a, b);
    }

    ;
//Interfejs Funkcyjny:


}
