package j8.model;

public enum ContractType {

    F("full") {

    },
    H("half") {

    };

    private String desc;

    ContractType(String desc) {
        this.desc = desc;
    }
}
