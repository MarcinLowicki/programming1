package j8.functionalInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class PlayWithConsumer {

    public static void main(String[] args) {

        Consumer<String> consumer = (String t) -> System.out.println(t);
        Consumer<String> consumer1 = System.out::print;

        consumer.accept("Jakis tam string");

        List<String> stringList = new ArrayList<>(Arrays.asList("Kasia", "Asia", "Basia"));


        //wyczysc z listy wszystkie stringi
        Consumer<List<String>> listConsumer = (List<String> e) -> e.clear();
        listConsumer.accept(stringList);

        listConsumer.accept(stringList);
        System.out.println(stringList.size());

        MyConsumer<List<String>> addConsumer = (List<String> t) -> t.add("test");
        MyConsumer<List<String>> addConsumer2 = (List<String> t) -> t.add("Hello 2 Consumer");
        MyConsumer<List<String>> groupingConsumer = addConsumer.andThen(addConsumer2);

        groupingConsumer.accept(stringList);
        System.out.println(stringList);
    }

}
