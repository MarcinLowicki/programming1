package j8.functionalInterface;

import java.util.function.Predicate;

public class PlayWithPredicate {

    public static void main(String[] args) {

        Predicate<String> checkA = (String t) -> t.contains("A");
        checkA.test("Ania");
        //True
        System.out.println(checkA.test("Kasia"));
        //False
        System.out.println(checkA.test("Yeti"));

        Predicate<String> stringStream = (String t) -> t.isEmpty();
        //False
        System.out.println(stringStream.test("cos tam"));
        //True
        System.out.println(stringStream.test(""));

        Predicate<String> hasAAndEmptyCheck = checkA.and(stringStream);

        hasAAndEmptyCheck.test("test");



    }

}
