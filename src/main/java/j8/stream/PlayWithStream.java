package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;
import org.w3c.dom.ls.LSOutput;

import java.nio.file.Path;
import java.text.Collator;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class PlayWithStream {

    public static void main(String[] args) {

        List<Employee> employees = FileUtils.load(Path.of("src/main/resources/employees.csv"));
        // System.out.print(employees);

        //ZAD Szymon1
        //Bez Ctrl+Alt+V
        employees.stream()
                .filter((e -> e.getAge() > 50))
                .collect(Collectors.toList());
        //ZAD2 Szymon - klucze zduplikowane to leci exeption bo mapa sie nie utworzy
        /*Map<String, String> collect = employees.stream()
                .collect(
                        Collectors.toMap(e ->
                                        e.getFirstName(),
                                e -> e.getLastName())
                );*/
        // System.out.println(collect);

        ex1(employees);
        ex2(employees);
        ex3(employees);
        ex4(employees);
        ex5(employees);
        ex6(employees);
        ex7(employees);
        ex8(employees);
        ex9(employees);
        ex10(employees);
        ex11(employees);
        ex12(employees);

    }

    //EX1 - Wypisz osoby których wynagorodzenie jest w przedziale 2500-3199
    public static void ex1(List<Employee> employees) {
        getSalary2500under3199(employees)
                .collect(Collectors.toList());
    }

    private static Stream<Employee> getSalary2500under3199(List<Employee> employees) {
        return employees.stream()
                .filter(e -> e.getSalary() > 2500 && e.getSalary() < 3199);
    }

    //EX2
    public static void ex2(List<Employee> employees) {
        ageModulo2(employees);
    }

    private static void ageModulo2(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getAge() % 2 == 0)
                //.count()
                .forEach(System.out::println);
    }

    public static void ex3(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska"))
                .filter(e -> e.getType().equals(ContractType.F))
                .forEach(System.out::println);
    }

    public static void ex4(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getFirstName()) //lambe mozna zastapic na object referance
                .forEach(System.out::println);
    }

    public static void ex5(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getLastName())
                .map(e -> e.substring(e.length() - 3))
                .map(e ->
                        (e.endsWith("ski") || e.endsWith("ska")) //jezeli to da prawde to wykonaj to
                                ? e.toUpperCase() : e) //wykonaj to co jest za znakiem zapytania a jak nie to to co jest za dwukropkiem :e
                .forEach(System.out::println);
    }

    public static void ex6(List<Employee> employees) {
        employees.stream()
                //.map(e->e.getProfession())
                .map(Employee::getProfession)
                //.map(e->e.toUpperCase());
                .map(String::toUpperCase)
                .forEach(System.out::println);
    }

    public static void ex7(List<Employee> employees) {
        System.out.println(isKowalski(employees));

    }

    private static boolean isKowalski(List<Employee> employees) {
        return employees.stream()
                .anyMatch(e -> e.getLastName().equals("Kowalski"));//anyMatch sprwadz jaki kolwiek czy jew Kowalski chociaz jeden
        //e.getLastName() =="Kowalski"  - to da FALSE bo sa to dwa rozne obiekty. Dwa rozne stringki
    }

    public static void ex8(List<Employee> employees) {
//Mapa<String, List<Double>> - lista unika duplikatow
//key extractor, downstream collector
        Map<String, List<Double>> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(//grupping by tworzy bez duplikatach
                                Employee::getLastName, //e -> e.getLastName(),//String
                                mapping(
                                        e -> e.getSalary() * 1.12, toList()
                                )
                        )
                );
        System.out.println(collect);
    }


    public static void ex9(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getFirstName().charAt(1) == 'a') //charAt
                .filter(e -> e.getLastName().charAt(2) == 'd')
                .forEach(System.out::println);
    }

    //porownaj dwa obiekty i posortuj dwa porownania
    public static void ex10(List<Employee> employees) {
        //porowanania
        employees.stream()
                .sorted(
                        Comparator.comparing(Employee::getLastName)
                                .thenComparing(Employee::getFirstName)
                )
                .forEach(System.out::println);
                        /*
                        (person1, person2) -> {
                            int lastNameComparator =
                                    person1.getLastName() //kowalski
                                            .compareTo(person2.getLastName()); //porownaj (abrahamowicz//
                            if (lastNameComparator != 0) {
                                return lastNameComparator;
                            }
                            return person1.getFirstName().compareTo(person2.getFirstName());
                        }
                ).forEach(System.out::println);
*/
    }

    public static void ex11(List<Employee> employees) {
        String collect = employees.stream()
                .map(e -> e.getLastName())//funkcja map z elementu daj mi nazwisko
                .collect(Collectors.joining(","));
        System.out.println(collect);
    }

    public static void ex12(List<Employee> employees) {
        //Map<Boolean, List<String>> collect = employees.stream() //list Stringow to moja profession
        Map<Boolean, Long> collect = employees.stream()
                //.map(e->e.getSalary()>5000)
                .collect(
                        Collectors.partitioningBy(
                                e -> e.getSalary() > 5000, Collectors.counting()
                                //Collectors.mapping(e->e.getProfession(), toList())));
                        )
                );
        System.out.println(collect);

    }

    //.colecor mapping/averageDouble wjezdzasz jkie Employee wyjezdzasz jaki double
    public static void ex13(List<Employee> employees) {
        Map<String, Double> collect = employees.stream()
                .collect(groupingBy(
                        e -> e.getType().name(),
                        averagingDouble(
                                e -> e.getSalary())

                        )
                );
        System.out.println(collect);


    }

    public static void ex14(List<Employee> employees) {

        Map<String, List<Employee>> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getFirstName()
                        )
                );
        System.out.println(collect);
    }

    public static void ex14A(List<Employee> employees) {

        Map<String, Long> collect = employees.stream()
                .collect(
                        groupingBy(e -> e.getFirstName(), counting()));

    }

    //ublic
//stworz mi mape: groupingBy(
    static void ex14C(List<Employee> employees) {

        Map<String, Long> a = employees.stream()
                //sortowanie Comparatorem
                .sorted( Comparator.comparing(Employee::getLastName))
                .collect(
                        groupingBy(
                                e -> e.getFirstName(),
                                filtering(e -> e.getFirstName().endsWith("a"),
                                        counting()
                                )
                        )
                );
    }
}

