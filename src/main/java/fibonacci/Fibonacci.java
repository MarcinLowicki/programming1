package fibonacci;

import org.w3c.dom.ls.LSOutput;

public class Fibonacci {

    public static void main(String[] args) {
        System.out.println(fibonacci(5));
    }

    static int fibonacci(int n) {
         int wynik = 0;


        int m = n;
        while (wynik <= m) {
            int a = m;
            int b = m + 1;
            if (n >= 1) {
                wynik = a + b;
                System.out.println(wynik);
            }
        }
        return wynik;
    }

}
