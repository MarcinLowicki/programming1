package lockZamek;

import java.util.Random;

public class Lock {

    private int correct1;
    private int correct2;
    private int correct3;

    private int current1;
    private int current2;
    private int current3;

    public Lock(int correct1, int correct2, int correct3) {
        this.correct1 = correct1;
        this.correct2 = correct2;
        this.correct3 = correct3;
    }


    public void switch1() {
        if (current1 == 9) {
            current1 = 0;
        } else {
            current1++;
        }

    }

    public void switch2() {
        current2 = (current3 + 1) % 10;
    }

    public void switch3() {
        current3 = (current3 + 1) % 10;

    }

    boolean isOpen() {
        System.out.println("Sprawdzam czy zamek jest otwarty");
        if (current1 == correct1 && current2 == correct2 && current3 == correct3) {
            System.out.println("It's open");
            return true;
        } else {
            System.out.println("it's not open");
            return false;
        }
    }

    public void shuffle() {
        Random random = new Random();
        int current1 = random.nextInt(10);
        int current2 = random.nextInt(10);
        int current3 = random.nextInt(10);
        System.out.println(current1 + " " + current2 + " " + current3);

    }

    @Override
    public String toString() {
        return "Aktualna kombinacja zamka: " +
                current1 + "-" + current2 + "-" + current3 + "-";
    }

}
